Un ejemplo de galería responsive basada en una sola línea con grid. En el repositorio sólo se contemplan los contenidos de desarrollo, los de **dist**ribución se generan al arrancar el proyecto con gulp.

Las tareas necesarias para compilar los archivos `pug` y `sass`, optimizar las imágenes y monitorizar los cambios se lanzan con un simple script de node:

````bash
npm run start
````

Antes generar el proyecto desde el repositorio clonado desde el remoto hay que vaciar la carpeta de imágenes, arrancarlo con el script mencionado y ya se pueden incluir de nuevo las mismas en esa misma carpeta. Así se ejecuta su optimización y colocación en el destino final para distribución.

